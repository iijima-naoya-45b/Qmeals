# レシピ1
Recipe.create(
  id: 1,
  user_id: 1,
  title: '美味しいカレーライス',
  content: 'あのカレーが短時間でできる！？',
  cooking_time: 10
)

Recipe.create(
  id: 2,
  user_id: 2,
  title: 'たこわさ',
  content: 'ビールのお供に！！',
  cooking_time: 5
)
