# This is the TermsOfServicesController class. It handles requests related to the terms of services page.
class TermsOfServicesController < ApplicationController
  skip_before_action :require_login
  # Controller actions and logic here
end
